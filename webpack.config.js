const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path');
const webpack = require('webpack');


module.exports = {
  context: path.resolve(__dirname, './src'),
  entry: './index',
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'bundle.js',
    publicPath: '/assets/'
  },
  module: {
    rules: [{
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          query: {
            presets: ["latest", 'react', 'stage-0']
          }
        }

      },
      {
        test: /\.jsx$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          query: {
            presets: ["latest", 'react', 'stage-0']
          }
        }

      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: '../public/',
        to: './',
        toType: 'dir'
      }
    ])
  ],
  resolve: {
    extensions: ['.js', '.jsx']
  }
}