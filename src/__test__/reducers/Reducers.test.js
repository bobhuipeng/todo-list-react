import types from '../../constants/ActionTypes'
import reducers from '../../reducers/Reducers'

import {defaultState} from '../../constants/SysConfig'

describe('todo list reducer', () => {

  const todos = [
    {
      "userId": 1,
      "id": 1,
      "title": "delectus aut autem",
      "completed": false
    },
    {
      "userId": 1,
      "id": 2,
      "title": "quis ut nam facilis et officia qui",
      "completed": true
    },
    {
      "userId": 2,
      "id": 3,
      "title": "et porro tempora",
      "completed": false
    }
  ]
  
  it('should return the initial state', () => {
    expect(reducers(undefined, {})).toEqual(defaultState)
  })
  
  it('should handle fetch todos', () => {
    //fetch todos request
    expect(
      reducers(defaultState, {
        type: types.FETCH_TODOS_REQUEST,
      })
    ).toEqual(
      {...defaultState,
        isFetching: true,
      }
    )

    //fetch todo success
    expect(
      reducers(defaultState, {
        type: types.FETCH_TODOS_SUCCESS,
        payload: todos
      })
    ).toEqual(
      {...defaultState,
        isFetching: false,
        userId: todos[0].userId,
        todos,
      }
    )
    //fetch todo fail
    const error = "some error"
    expect(
      reducers(defaultState, {
        type: types.FETCH_TODOS_FAILURE,
        error
      })
    ).toEqual(
      { todos: [],
        isFetching: false,
        error,
      }
    )
  })

  it('should handle update user id', () => {
    //fetch todo fail
   const userId = 2;
    expect(
      reducers(defaultState, {
        type: types.UPDATE_USER_ID,
        payload: userId
      })
    ).toEqual(
      { ...defaultState,
       userId
      }
    )
})

})