import React from 'react'
import renderer from 'react-test-renderer'
import ShallowRenderer from 'react-test-renderer/shallow'; 
import 'jest-styled-components'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import TodoTable from '../../components/TodoTable'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

const shallowRenderer = new ShallowRenderer();

describe('TodoTable component', () => {
  const todos = [
    {
      "userId": 1,
      "id": 1,
      "title": "delectus aut autem",
      "completed": false
    },
    {
      "userId": 1,
      "id": 2,
      "title": "quis ut nam facilis et officia qui",
      "completed": true
    },
    {
      "userId": 2,
      "id": 3,
      "title": "et porro tempora",
      "completed": false
    }
  ]

  const store = mockStore({ todos: [] })
  const wrappedTodoTable = <Provider store = {store}><TodoTable todos={todos}/></Provider>

  it('should match snapshot', () => {
    const result = shallowRenderer.render(<TodoTable todos={todos}/>)
    expect(result).toMatchSnapshot()
  });

  it('should has styles', () => {
    const todoTableContainer = renderer.create( wrappedTodoTable).toJSON()
    expect(todoTableContainer).toHaveStyleRule('position','relative')
    expect(todoTableContainer).toHaveStyleRule('background-color','white')
    expect(todoTableContainer).toHaveStyleRule('font-size','13px')
  });

  it('should has props todo as same as input', () => {
    const todoTableContainer = renderer.create( wrappedTodoTable).root
    expect(todoTableContainer.findByType(TodoTable).props.todos).toBe(todos)
  });

});