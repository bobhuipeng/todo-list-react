import React from 'react'
import renderer from 'react-test-renderer'
import ShallowRenderer from 'react-test-renderer/shallow'; 
import 'jest-styled-components'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import TodoList from '../../components/TodoList'
// import {defaultState} from '../../constants/SysConfig'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

const shallowRenderer = new ShallowRenderer();

describe('TodoList component', () => {
  const todos = [
    {
      "userId": 1,
      "id": 1,
      "title": "delectus aut autem",
      "completed": false
    },
    {
      "userId": 1,
      "id": 2,
      "title": "quis ut nam facilis et officia qui",
      "completed": true
    },
    {
      "userId": 2,
      "id": 3,
      "title": "et porro tempora",
      "completed": false
    }
  ]

  const defaultState = {
    isFetching: false,
    userId: 1,
    todos
  }



  const store = mockStore(defaultState)
  const wrappedTodoList = <Provider store = {store}><TodoList getTodoList={jest.fn(() => todos)} todos={todos} /></Provider>

  it('should match snapshot', () => {
    const result = shallowRenderer.render(<TodoList getTodoList={jest.fn(() => todos)} todos={todos}/>)
    expect(result).toMatchSnapshot()
  });

  it('should has styles', () => {
    const titleContainer = renderer.create(wrappedTodoList).toJSON()
    expect(titleContainer).toHaveStyleRule('max-width','350px')
    expect(titleContainer).toHaveStyleRule('max-height','280px')
    expect(titleContainer).toHaveStyleRule('overflow',"auto")
    expect(titleContainer).toHaveStyleRule('left',"0")
    expect(titleContainer).toHaveStyleRule('right',"0")
    expect(titleContainer).toHaveStyleRule('margin',"auto")

  });

  it('should apply default value to title name', () => {
    const loadSize = 10
    const titleContainer = renderer.create( wrappedTodoList).root
    expect(titleContainer.findByType(TodoList).props.loadSize).toBe(loadSize)
  });


  it('should apply props value to title name', () => {
    const loadSize = 5
    const titleContainer = renderer.create( <Provider store = {store}><TodoList loadSize={loadSize}  getTodoList={jest.fn(() => todos)} todos={todos}/></Provider>).root
    expect(titleContainer.findByType(TodoList).props.loadSize).toBe(loadSize)
  });
});