import React from 'react'
import renderer from 'react-test-renderer'
import ShallowRenderer from 'react-test-renderer/shallow'; 
import 'jest-styled-components'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import Title from '../../components/Title'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

const shallowRenderer = new ShallowRenderer();

describe('Title component', () => {
  const store = mockStore({ todos: [] })
  const wrappedTitle = <Provider store = {store}><Title /></Provider>

  it('should match snapshot', () => {
    const result = shallowRenderer.render(<Title />)
    expect(result).toMatchSnapshot()
  });

  it('should has styles', () => {
    const titleContainer = renderer.create( wrappedTitle).toJSON()
    expect(titleContainer).toHaveStyleRule('position','absolute')
    expect(titleContainer).toHaveStyleRule('background-color','white')
    expect(titleContainer).toHaveStyleRule('text-align','center')
    expect(titleContainer).toHaveStyleRule('z-index',"99")
    expect(titleContainer).toHaveStyleRule('opacity',"1")
  });

  it('should apply default value to title name', () => {
    const titleName = 'Todo List'
    const titleContainer = renderer.create( wrappedTitle).root
    expect(titleContainer.findByType(Title).props.titleName).toBe(titleName)
  });


  it('should apply props value to title name', () => {
    const titleName = 'New Todo List'
    const titleContainer = renderer.create( <Provider store = {store}><Title titleName={titleName} store = {store}/></Provider>).root
    expect(titleContainer.findByType(Title).props.titleName).toBe(titleName)
  });
});