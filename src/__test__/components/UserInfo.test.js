import React from 'react'
import renderer from 'react-test-renderer'
import 'jest-styled-components'

import UserInfo from '../../components/UserInfo'



describe('UserInfo component', () => {
  it('should match snapshot', () => {
    const userInfoContainer = renderer.create( <UserInfo userId={1}/>).toJSON()
    expect(userInfoContainer).toMatchSnapshot()
  });

  it('should has styles', () => {
    const userInfoContainer = renderer.create( <UserInfo userId={1}/>).toJSON()
    expect(userInfoContainer).toHaveStyleRule('font-size','0.7em')
    expect(userInfoContainer).toHaveStyleRule('text-transform','uppercase')
  });

  it('should apply userId in element', () => {
    const userId = 1
    const userInfoContainer = renderer.create( <UserInfo userId={userId}/>).root
    expect(userInfoContainer.props.userId).toBe(userId)
  });
});