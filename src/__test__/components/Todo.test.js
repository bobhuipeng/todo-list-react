import React from 'react'
import renderer from 'react-test-renderer'
import 'jest-styled-components'

import Todo from '../../components/Todo'



describe('Todo component', () => {

  const incompletedTdo = {
    "userId": 1,
    "id": 1,
    "title": "delectus aut autem",
    "completed": false
  }

  const completedTodo = {
    "userId": 1,
    "id": 1,
    "title": "delectus aut autem",
    "completed": true
  }

  it('should match snapshot for complete todo', () => {
    const TodoContainer = renderer.create( <Todo index={1} updateUserId={jest.fn(() => 1)} {...completedTodo}/>).toJSON()
    expect(TodoContainer).toMatchSnapshot()
  });

  it('should match snapshot for incomplete todo', () => {
    const TodoContainer = renderer.create( <Todo index={1} updateUserId={jest.fn(() => 1)} {...incompletedTdo}/>).toJSON()
    expect(TodoContainer).toMatchSnapshot()
  });

  it('should has styles', () => {
    const TodoContainer = renderer.create( <Todo index={1} updateUserId={jest.fn(() => 1)} {...completedTodo}/>).toJSON()
    expect(TodoContainer).toHaveStyleRule('display','inline-flex')
    expect(TodoContainer).toHaveStyleRule('border-style','solid')
    expect(TodoContainer).toHaveStyleRule('width','92%')
  });  
});