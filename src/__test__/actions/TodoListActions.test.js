import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import moxios from 'moxios'


import * as actions  from '../../actions/TodoListActions'
import types from '../../constants/ActionTypes'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)


const todos = [
  {
    "userId": 1,
    "id": 1,
    "title": "delectus aut autem",
    "completed": false
  },
  {
    "userId": 1,
    "id": 2,
    "title": "quis ut nam facilis et officia qui",
    "completed": true
  },
  {
    "userId": 2,
    "id": 3,
    "title": "et porro tempora",
    "completed": false
  }
]


describe('Todo list actions',() => {
  beforeEach(function () {
    moxios.install()
  })

  afterEach(function () {
    moxios.uninstall()
  })

  it('creates FETCH_TODOS_SUCCESS when fetching todos has been done',  () => {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: todos,
      });
    });

    const expectedActions = [
      { type: types.FETCH_TODOS_REQUEST },
      { type: types.FETCH_TODOS_SUCCESS, payload:  todos }
    ]

    const store = mockStore({ todos: [] })

    return store.dispatch(actions.fetchTodos(10)).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions);
    })

  })


  it('should update userId ', () => {
    const userId = 2
    const expectedAction = {
      type: types.UPDATE_USER_ID,
      payload: userId
    }
  
    expect(actions.updateUserId(userId)).toEqual(expectedAction)
  });


})

