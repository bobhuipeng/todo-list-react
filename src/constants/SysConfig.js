export const baseUrl = 'https://jsonplaceholder.typicode.com';

export const defaultState = {
  isFetching: false,
  userId: null,
  todos: []
}