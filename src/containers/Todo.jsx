import { connect } from 'react-redux'

import Todo from '../components/Todo'
import { updateUserId } from '../actions/TodoListActions'

const mapStateToProps = (state, ownProps)  => ({
  ...ownProps,
})

const mapDispatchToProps = dispatch => ({
  updateUserId(userId) {
    dispatch(
      updateUserId(userId)
    )
  }
})


export default connect(mapStateToProps,mapDispatchToProps)(Todo)