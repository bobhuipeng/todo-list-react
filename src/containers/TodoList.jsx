import { connect } from 'react-redux'

import TodoList from '../components/TodoList'
import { fetchTodos } from '../actions/TodoListActions'

const mapStateToProps = state => ({
  todos: state.todos
})

const mapDispatchToProps = dispatch => ({
  getTodoList(maxSize) {
    dispatch(
      fetchTodos(maxSize)
    )
  }
})


export default connect(mapStateToProps,mapDispatchToProps)(TodoList)