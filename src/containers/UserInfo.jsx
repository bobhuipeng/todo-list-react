import { connect } from 'react-redux'

import UserInfo from '../components/UserInfo'

const mapStateToProps = (state)  => ({
  userId: state.userId
})

export default connect(mapStateToProps)(UserInfo)