import ActionTypes from '../constants/ActionTypes'

import {defaultState} from '../constants/SysConfig'

const todoList = (state = defaultState , action) => {

  switch (action.type) {
    case ActionTypes.FETCH_TODOS_REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case ActionTypes.FETCH_TODOS_SUCCESS:
      return {
        todos: action.payload,
        isFetching: false,
        userId: action.payload[0].userId
      };
    case ActionTypes.FETCH_TODOS_FAILURE:
      return {
        todos: [],
        error: action.error,
        isFetching: false
      };      case ActionTypes.UPDATE_USER_ID:
      return {
        ...state,
        userId: action.payload
      };
    default:
      return state;
  }

}

export default todoList