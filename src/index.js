import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';
import logger,{ createLogger }  from 'redux-logger'

import reducers from './reducers/Reducers'

let middlewares = [thunk]

if (process.env.NODE_ENV === `development`) {
  middlewares.push(logger);
  createLogger({level :'info'})
}

const store = createStore(reducers,composeWithDevTools(applyMiddleware(...middlewares)))


ReactDOM.render(
  <Provider store = {store}>
    <App />
  </Provider>, 
  document.getElementById('root')
);
registerServiceWorker();
