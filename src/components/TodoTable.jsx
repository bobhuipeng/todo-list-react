import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Todo from '../containers/Todo'

export default class TodoTable extends Component {
  static propTypes = {
    todos:  PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      completed: PropTypes.bool
    })).isRequired  
  }


  render() {
    //TODO: add error boundary 
    return (
      <TodoTableStyle>
        {this.props.todos.map((todo,index) =>{
          return <Todo key={todo.id} index={index+1} {...todo}/>
        })}
      </TodoTableStyle>
    )
  }
}


const TodoTableStyle = styled.div`
  position: relative;
  background-color: white;
  margin-top:4.85em;
  font-size: 13px;
`
