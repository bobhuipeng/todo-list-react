import React, { Component } from 'react'
import PropTypes from 'prop-types'

import styled from 'styled-components'

import Title from './Title'
import TodoTable from './TodoTable'


export default class TodoList extends Component {

  static propTypes = {
    loadSize: PropTypes.number,
    getTodoList: PropTypes.func.isRequired
  }

  static defaultProps = {
    loadSize: 10
  };

  
  componentDidMount() {
    this.props.getTodoList(this.props.loadSize)
  }

  

  render() {
    return (
      <TodoListStyle>
        <Title/>
        <TodoTable todos={this.props.todos}/>
      </TodoListStyle>
    )
  }
}


const TodoListStyle = styled.div`
  max-width: 350px;
  max-height: 280px;
  overflow: auto;
  border: 2px solid rgb(209,209,209);
  border-radius: 8px;
  left: 0;
  right: 0;
  margin: auto;
`