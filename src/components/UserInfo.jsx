import React from 'react'
import PropTypes from 'prop-types'

import styled from 'styled-components'

const UserInfo = ({userId}) => (
      <UserInfoStyle>
        User Id: {userId}
      </UserInfoStyle>
      )

UserInfo.propTypes = {
  userId: PropTypes.number
}


const UserInfoStyle = styled.div`
  font-size: 0.7em;
  text-transform: uppercase;
`
export default UserInfo