import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import UserInfo from '../containers/UserInfo'
import TitleTextStyle from './styles/TitleTextStyle'

export default class Title extends Component {
  static propTypes = {
    titleName: PropTypes.string
  }

  static defaultProps = {
    titleName: 'Todo List'
  };

  render() {
    return (
      <TitleStyle>
        <TitleTextStyle>
          {this.props.titleName}
        </TitleTextStyle>
        <UserInfo/>
      </TitleStyle>
    )
  }
}

const TitleStyle = styled.div`
  position: absolute;
  top:0.5em;
  height: 3em;
  width:350px;
  z-index: 99;
  opacity:1;
  background-color:white;
  text-align: center;
  margin-top: 2px;
  border-bottom:2px solid rgb(209,209,209);
  border-radius: 8px 8px 0 0;
  padding-top: 0.5em;
`
