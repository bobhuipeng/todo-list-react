import React from "react";
import PropTypes from "prop-types";
import styled from 'styled-components'

import IndexStyle from './styles/IndexStyle'

const Todo = ({ index, title, completed, userId, updateUserId }) => {
  let color = completed ? '#3dc4a6' : '#ca2f51'
  let bgcolor = completed ? 'white' : '#fff1f4'
  let bgcolorOnHover = completed ? '#a2fbe6' : '#fcbac6'
  return ( 
    <TodoStyle color={color} bgcolor={bgcolor} bgcolorOnHover={bgcolorOnHover}  onClick={()=>updateUserId(userId)}>
      <IndexStyle color={color}> <div>{index}</div> </IndexStyle>
      <TodoContentStyle>
        <TodoTitleStyle color={color}> Title </TodoTitleStyle>
        <DisplyTitleStyle>{title}</DisplyTitleStyle>
      </TodoContentStyle>
    </TodoStyle>
  )
 }

Todo.propTypes = {
  index: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  completed: PropTypes.bool.isRequired,
  id: PropTypes.number.isRequired,
  updateUserId: PropTypes.func.isRequired
};


const TodoStyle = styled.div.attrs({
  color: props =>  props.color,
  bgcolor:  props => props.bgcolor,
  bgcoloronhover: props => props.bgcolorOnHover,
})`
  display: inline-flex;
  margin-bottom: 0.5em;
  width: 92%;
  border-color:${props => props.color};
  border-width: 2px;
  border-style: solid;
  border-radius: 5px;
  background-color: ${props => props.bgcolor};
  margin-left: 0.8em;
  &:hover {
    background-color:${props => props.bgcoloronhover};  
    }
`

const TodoContentStyle = styled.div`
  align-items: center;
  margin-top:1em;
  width: 80%;
`

const TodoTitleStyle = styled.div.attrs({
  color: props => props.color
})`
  text-align: left;
  text-transform: uppercase;
  font-size: 0.8em;
  font-weight: bold;
  color: ${props => props.color};
`

const DisplyTitleStyle = styled.div`
  white-space: nowrap;
  overflow:hidden;
  text-overflow: ellipsis;
  &:hover {
    white-space: normal;
    text-overflow: inherit; 
    overflow: visible;
  }
`

export default Todo