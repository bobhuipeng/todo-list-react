
import styled from 'styled-components'

const IndexStyle = styled.div.attrs({
  color: props => props.color
})`
  height: 3.2em;
  width:3.2em;
  line-height: 3.2em;
  background-color:${props => props.color};
  color: white;
  text-align: center;
  margin: 0.5em;
  border-radius: 5px;
`


export default IndexStyle