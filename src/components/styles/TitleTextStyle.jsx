
import styled from 'styled-components'

const TitleTextStyle = styled.div`
  text-transform: capitalize;
  font-weight: bold;
`

export default TitleTextStyle