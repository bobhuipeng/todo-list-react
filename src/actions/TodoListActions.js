import axios from 'axios'

import ActionTypes from '../constants/ActionTypes'

import {baseUrl} from '../constants/SysConfig'

const fetchTodosRequest = () => {
  return {
    type: ActionTypes.FETCH_TODOS_REQUEST
  }
}

const fetchTodosSuccess = (payload) => {
  return {
    type: ActionTypes.FETCH_TODOS_SUCCESS,
    payload
  }
}

const fetchTodosFailure = (error) => {
  return {
    type: ActionTypes.FETCH_TODOS_FAILURE,
    error
  }
}





//fetch data from remote service. 
//maxSize: is the max size of todo collection will be returned
export const fetchTodos = (maxSize) => {
  return async dispatch => {
    try{
      dispatch(fetchTodosRequest())
      const res = await axios.get(`${baseUrl}/todos`)
      let todos = res.data.size <=maxSize ? res.data : res.data.slice(0,maxSize)
      return dispatch(fetchTodosSuccess(todos))
    }catch(error){
      dispatch(fetchTodosFailure(error))
    }

  }
}

export const updateUserId = (userId) => {
  return {
    type: ActionTypes.UPDATE_USER_ID,
    payload:userId
  }
}
